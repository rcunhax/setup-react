import * as React from 'react';

interface FileUploadProps {
    multiple?: boolean;
    showPreview?: boolean;
    accept?: string;
    files?: File[];
    onFiles?: (files: File[]) => void;
    filesPreview?: string[];
    filesNames?: string[];
}

class FileUpload extends React.Component<FileUploadProps> {

    state: FileUploadProps = { };
    fileInput;
    emptyBlock;
    files: File[] = [];
    wrapper;

    constructor(props: FileUploadProps) {
        super(props);
    }

    componentDidMount() {
        console.log(document);
        document.addEventListener('drop', (event) => {
            console.log('document.addEventListener');

            event.preventDefault();
            event.stopPropagation();
            console.log(event);
        });
        
        window.addEventListener('drop', (event) => {
            console.log('window.addEventListener');
            event.preventDefault();
            event.stopPropagation();
            console.log(event);
        });
        this.fileInput = React.createRef<HTMLInputElement>();
        this.wrapper = React.createRef<HTMLDivElement>();

        this.emptyBlock = (
            <div>
                {this.props.children}
                <button 
                    className="btn"
                    type="button"
                    onClick={() => this.triggerClick()}
                >
                    Click
                </button>
            </div> 
        );

        this.setState({
            accept: this.props.accept || '*',
            multiple: this.props.multiple
        });
    }

    processFiles (fileList: FileList) {
        let filesPreview = [];
        let filesNames = [];
        let files = [];
        if (this.props.multiple) {
            filesPreview = this.state.filesPreview || [];
            filesNames = this.state.filesNames || [];
            files = this.state.files || [];
        }
        
        for (let i = 0; i < fileList.length; i++) {
          const file = fileList.item(i);
          files.push(file);
          if (this.props.showPreview) {
            filesPreview.push(URL.createObjectURL(file));
          } else {
            filesNames.push(file.name);
          }
          if (!this.state.multiple && i === 0) {
            break;
          }
        }
        this.setState({
            filesPreview: filesPreview,
            filesNames: filesNames,
            files: files
        });
        this.props.onFiles(files);
    }

    deleteFile(index: number) {
        const files = this.state.files;
        const filesNames = this.state.filesNames;
        const filesPreview = this.state.filesPreview;

        files.splice(index, 1);
        filesNames.splice(index, 1);
        filesPreview.splice(index, 1);
        
        this.setState({
            filesPreview: filesPreview,
            filesNames: filesNames,
            files: files
        });
        this.props.onFiles(files);
    }


    fileChangeHandler (event: any) {
        event.preventDefault();
        event.stopPropagation();
        const fileList = event.target.files as FileList;
        this.processFiles(fileList);
    }

    onDrop (event: any) {
        event.preventDefault();
        event.stopPropagation();
        console.log('onDrop');

    }

    onDragEnter(event: any) {
        event.preventDefault();
        event.stopPropagation();
        // console.log(this.wrapper);
        console.log('onDragEnter');
    }

    onDragLeave(event: any) {
        event.preventDefault();
        event.stopPropagation();
        console.log('onDragLeave');
    }

    onDragEnd(event: any) {
        event.preventDefault();
        event.stopPropagation();
        console.log('onDragEnd');
    }
    onDragStart(event: any) {
        event.preventDefault();
        event.stopPropagation();
        console.log('onDragStart');
    }

    onDrag(event: any) {
        event.preventDefault();
        event.stopPropagation();
        console.log('onDrag');
    }


    triggerClick() {
        this.fileInput.current.click();
    }

    
    render() {
        return (
            <div 
                ref={this.wrapper} 
                className="file-upload-area text-center" 
                // onDrop={(e) => this.onDrop(e)}
                // onDragEnter={(e) => this.onDragEnter(e)}
                // onDragLeave={(e) => this.onDragLeave(e)}
                // onDragEnd={(e) => this.onDragEnd(e)}
                // onDragStart={(e) => this.onDragStart(e)}
                // onDrag={(e) => this.onDrag(e)}
            >
                {/* <div 
                    className="file-upload-mask"
                    onDrop={(e) => this.onDrop(e)}
                    onDragEnter={(e) => this.onDragEnter(e)}
                    onDragLeave={(e) => this.onDragLeave(e)}
                    onDragEnd={(e) => this.onDragEnd(e)}
                    onDragStart={(e) => this.onDragStart(e)}
                    onDrag={(e) => this.onDrag(e)}
                
                /> */}
                <input
                    type="file"
                    accept={this.state.accept}
                    multiple={this.state.multiple}
                    ref={this.fileInput}
                    onChange={(e) => this.fileChangeHandler(e)} 
                />
                {
                    (this.state.filesPreview && this.state.filesPreview.length) ? 
                        <div className="d-flex flex-wrap justify-content-center my-2">
                            {
                                this.state.filesPreview.map((url: string, index: number) => (
                                    <div className="file-preview text-center m-1 p-1" key={index}>
                                        <img className="mb-2" src={url}/>
                                        <button 
                                            className="btn btn-sm btn-warning"
                                            type="button"
                                            onClick={() => this.deleteFile(index)}
                                        >
                                            Delete
                                        </button>
                                    </div>   
                                ))
                            }
                        </div>
                    : ''
                }
                {
                    (this.state.filesNames && this.state.filesNames.length) ? 
                        <div className="d-flex flex-wrap justify-content-center my-2">
                        (
                            {
                                this.state.filesNames.map((name: string, index: number) => (
                                    <div className="file text-center">
                                        <span>{name}</span>
                                        <br/>
                                        <button 
                                            className="btn btn-sm btn-warning"
                                            type="button"
                                            onClick={() => this.deleteFile(index)}
                                        >
                                            Delete
                                        </button>
                                    </div>
                                ))
                            }
                        )
                        </div>
                    : ''
                }
                {
                    ((!this.state.filesNames || !this.state.filesNames.length) && (!this.state.filesPreview || !this.state.filesPreview.length)) ?
                    (this.emptyBlock) : ''
                }

            </div>
            );
        }
}

export default FileUpload;
