import * as React from 'react';
import { ITemplate, IAppStateProps } from '../../model/models';
import Template from './template';
import Aux from '../hoc/auxiliar';
import { mapStateToProps } from '../../constants';
import { connect } from 'react-redux';

interface TemplateListProps {
    onEdit?: (e: any, template: ITemplate) => void;
    onDelete?: (e: any, template: ITemplate) => void;
    select?: (e: any, template: ITemplate) => void;
    templates?: ITemplate[];
    previewTemplate?: (e: any, template: ITemplate) => void;
}
type AllProps = TemplateListProps & IAppStateProps;

const TemplateList: React.SFC<AllProps> = (props) => {

    const onEdit = (e: any, template: ITemplate) => {
        if (props.onEdit && props.isPowerUser) {
            props.onEdit(e, template);
        }
    };

    const onDelete = (e: any, template: ITemplate) => {
        if (props.onDelete && props.isPowerUser) {
            props.onDelete(e, template);
        }
    };

    const select = (e: any, template: ITemplate) => {
        if (props.onEdit) {
            props.select(e, template);
        }
    };

    return (
        <Aux>
            {
                (props.templates && props.templates.length) ?
                    props.templates.map((template: ITemplate) => (
                        <div key={template.id} className="col-lg-4 col-md-6 mb-4">
                            <div data-id={template.id}>
                                <Template 
                                    template={template} 
                                    select={(e) => select(e, template)}
                                    onEdit={(e) => onEdit(e, template)}
                                    onDelete={(e) => onDelete(e, template)}
                                    previewTemplate={(e) => props.previewTemplate(e, template)}
                                />
                            </div>
                        </div>
                    ))
                    :
                    <h6 className="text-muted text-center col-12 mb-4">
                        No Templates
                    </h6>
            }
        </Aux>
    );
};
export default connect(mapStateToProps, null)(TemplateList);
