import * as React from 'react';
import { ITemplate, IAppStateProps } from '../../model/models';
import { SERVER_FILES_URL, mapStateToProps } from '../../constants';
import Aux from '../hoc/auxiliar';
import { connect } from 'react-redux';

interface TemplateProps {
    template: ITemplate;
    displayOnly?: boolean;
    onEdit?: (e: any, template: ITemplate) => void;
    onDelete?: (e: any, template: ITemplate) => void;
    select?: (e: any, template: ITemplate) => void;
    previewTemplate?: (e: any, template: ITemplate) => void;
}
type AllProps = TemplateProps & IAppStateProps;

const Template: React.SFC<AllProps> = (props) => {  
    const template = props.template;
    let editButton;
    if (props.onEdit && props.isPowerUser) {
        editButton = (
        <button 
            type="button" 
            className="btn btn-sm btn-warning edit-button" 
            onClick={(e) => props.onEdit(e, template)}
        >
            <i className="fa fa-pencil"/>
        </button>
        );
    }
    let deleteButton;
    if (props.onDelete && props.isPowerUser) {
        deleteButton = (
        <button 
            type="button" 
            className="btn btn-sm btn-danger delete-button ml-1" 
            onClick={(e) => props.onDelete(e, template)}
        >
            <i className="fa fa-trash"/>({template.id})
        </button>
        );
    }

    let warning;
    
    if (!template.templateFileId || +template.templateFileId === 0) {
        console.log(template.templateFileId);

        warning = (
        <button 
            type="button"
            title="No Template"
            className="btn btn-sm btn-danger  ml-1" 
        >
            <i className="fa fa-info"/>
        </button>
        );
    }
    const actionButtons = (
        <div className="action-buttons">
           {editButton}{deleteButton} {warning} 
        </div>
    );
    const backgroundImageStyle = {
        backgroundImage: `url(${SERVER_FILES_URL}/${template.templateImageId})`
    };

    return (        
        <div className="template">
            <div className="template-image" style={backgroundImageStyle}  >

                {!props.displayOnly ?
                    (
                        <Aux>

                            {props.isPowerUser ? actionButtons : ''}
                            <div className="template-overlay"/>
                            
                            <div className="template-actions">
                                <button 
                                    type="button" 
                                    className="btn btn-info select-btn m-1" 
                                    onClick={(e) => props.select(e, template)}
                                >
                                    Select 
                                </button>
            
                                <button 
                                    type="button" 
                                    className="btn btn-light preview-btn m-1" 
                                    onClick={(e) => props.previewTemplate(e, template)}
                                >
                                    Preview
                                </button>
                            </div>
                        </Aux>
                    )
                    :
                    ''
                }                
            </div>
            <div className="text-left my-2">
                <h5 className="mb-1">{template.name}</h5>
                <p className="text-muted line-height-1">{template.description}</p>
            </div>
        </div>
    );
};
export default connect(mapStateToProps, null)(Template);
