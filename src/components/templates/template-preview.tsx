import * as React from 'react';
import { ITemplate } from '../../model/models';

interface TemplatePreviewProps {
    template: ITemplate;
}

const TemplatePreview: React.SFC<TemplatePreviewProps> = (props) => {  
    const template = props.template;
    return (        
        <div className="template-preview">
            <iframe
                frameBorder="0"
                src={template.demoUrl ? template.demoUrl : 'http://localhost:3000'}
            />
        </div>
    );
};
export default TemplatePreview;