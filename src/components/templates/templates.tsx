import * as React from 'react';
import { AxiosResponse } from 'axios';
import { ICategory, ITemplate, IAppStateProps } from '../../model/models';
import { TEMPLATE_API_URLS, SERVER_FILES_URL, mapStateToProps } from '../../constants';
import TemplateForm from './template-form';
import { DbyHttp } from '../../http';
import Modal, { ModalSizes } from '../hoc/modal';
import TemplateList from './template-list';
import Aux from '../hoc/auxiliar';
import TemplatePreview from './template-preview';
import { connect } from 'react-redux';

interface TemplatesProps {
    select: (e: any, template?: ITemplate) => void; 
    category?: ICategory;
    categories?: ICategory[];
}

interface TemplatesState {
    category?: ICategory;
    template?: ITemplate; 
    templates?: ITemplate[];
    searchResult?: ITemplate[];
    searchTerm?: string;
    categories: ICategory[];
    templateOnPreview?: ITemplate; 
    formState: {
        uploadProgress?: number;
        saving?: boolean;
        errorMessage?: string;
    };
    
}

type AllProps = TemplatesProps & IAppStateProps;

class Templates extends React.Component<AllProps, TemplatesState> {

    state: TemplatesState = {
        categories: [],
        formState: {}
    };

    allCategories: ICategory[] = [];
    templateSearchTimeout: any;

    constructor(props: TemplatesProps) {
        super(props);
    }

    componentDidMount() {
        this.getAllTemplates();
    }

    componentWillReceiveProps(nextProps: Readonly<TemplatesProps>, nextContext: any) {        
        if (!this.allCategories || !this.allCategories.length) {
            this.allCategories = this.state.categories;
        }

        if (nextProps.category !== this.props.category) {
            this.setState({
                category: nextProps.category,
                categories: this.allCategories.filter((cat) => !nextProps.category || nextProps.category.id === cat.id)
            });
        } else {
            this.setState({
                categories: nextProps.categories
            });
        }
    }

    mapTemplatesToCategories(templates: ITemplate[], categories: ICategory[]) {
        const noCategory: ICategory = {
            id: 0,
            templates: []
        };

        templates.forEach(template => {
            template.imageUrl = `${SERVER_FILES_URL}/${template.templateImageId}`;
            if (template.category < 1) {
                noCategory.templates.push(template);
            }
        });

        if (categories) {
            categories.forEach(category => {
                category.templates = templates.filter(template => template.category === category.id);     
            }); 
        } else {
            categories = [];
        }
        
        if (noCategory.templates.length) {
            categories.push(noCategory);
        }

        this.setState({
            categories: categories
        });
    }

    getAllTemplates() {
        let categories = this.props.categories || [];
        DbyHttp.doGet(TEMPLATE_API_URLS.GET_TEMPLATES)
        .then((response: AxiosResponse<ITemplate[]>) => {            
            const templates = response.data;
            this.mapTemplatesToCategories(templates, categories);
        }).catch(() => {
            this.setState({
                categories: categories
            });
        });
    }

    closeTemplateForm() {
        this.setState({
            template: null
        });
    }
    
    editHandler(event: any, template: ITemplate) {
        this.setState({
            template: template
        });
    }

    deleteHandler (event: any, template: ITemplate) {
        DbyHttp.doDelete(`${TEMPLATE_API_URLS.DELETE_TEMPLATE}?id=${template.id}`)
        .then(() => {
            this.getAllTemplates();
        });
    }

    editTemplate (event: any, template: ITemplate) {
        this.setState({
            template: template
        });
    }

    templateChangeHandler(event: any, template?: ITemplate) {
        if (!template) {
            const name = event.target.name;
            const value = event.target.value;
            template = this.state.template;
            template[name] = value;  
        }
        this.setState({
            template: template
        });
    }

    saveTemplateHandler(event: any) {       
        event.preventDefault();
        const template: ITemplate = {
            active: true,
            category: this.state.template.category,
            createDate: this.state.template.createDate,
            description: this.state.template.description,
            id: this.state.template.id ? this.state.template.id : -1,
            location: this.state.template.location,
            modifiedDate: this.state.template.modifiedDate,
            name: this.state.template.name,
            templateFileId: this.state.template.templateFileId,
            templateImageId: this.state.template.templateImageId,
            uuid: this.state.template.uuid
        };
        if (!template.id) {
            template.id = -1;
        }

        const formData = new FormData();
        formData.append('dbySiteTemplateJson', JSON.stringify(template));

        let hasImage = false;
        let hasFile = false;
        if (this.state.template.fileFile) {
            hasFile = true;
            formData.append('templateFile', this.state.template.fileFile);
        }
        if (this.state.template.imageFile) {
            hasImage = true;
            formData.append('imageFile', this.state.template.imageFile);
        }

        let url = 'dby.dbysitetemplate/save-site-template-data';
        if (hasImage && hasFile) {
            formData.append('templateFile', this.state.template.fileFile);
            formData.append('imageFile', this.state.template.imageFile);
            url = 'dby.dbysitetemplate/save-site-template';
        } else if (hasImage) {
            formData.append('file', this.state.template.imageFile);
            url = 'dby.dbysitetemplate/save-site-template-one-file';
        } else if (hasFile) {
            formData.append('file', this.state.template.imageFile);
            url = 'dby.dbysitetemplate/save-site-template-one-file';
        }
        const formState = this.state.formState || {};
        formState.saving = true;
        this.setState({
            formState: formState
        });
        DbyHttp.doLiferayPost<ITemplate>(url, formData, (e) => this.uploadProgressHandler(e))
        .then(response => {
            this.getAllTemplates();
            this.setState({
                template: null,
                formState: {}
            });
        }).catch(() => {
            this.setState({
                formState: {
                    errorMessage: 'Error saving your template'
                }
            });
        });
        
    }

    uploadProgressHandler(event: ProgressEvent) {
        const formState = this.state.formState || {};
        formState.uploadProgress = Math.round(100 * (event.loaded / event.total));
        this.setState({
            formState: formState
        });
    }
   
    templateSearch(event: any) {
        const searchTerm = event.target.value;
        this.setState({
            searchTerm: searchTerm
        });
        if (searchTerm) {
           DbyHttp.doGet(TEMPLATE_API_URLS.FIND_BY_NAME + '?name=' + searchTerm)
            .then((response: AxiosResponse<ITemplate[]>) => {
                this.setState({
                    searchResult: response.data || []
                });
            }); 
        } else {
            this.clearSearch();
        }
        
    }

    clearSearch() {
        this.setState({
            searchResult: null
        });
    }

    closeTemplatePreview() {
        this.setState({
            templateOnPreview: null
        });
    }

    previewTemplate(e: any, template: ITemplate) {
        this.setState({
            templateOnPreview: template
        });
    }
        
    render() {

        return (
            <div>
                {
                    this.state.template && this.props.isPowerUser ?  
                    (
                        <Modal onClose={() => this.closeTemplateForm()}>
                            <h3>
                                {
                                    this.state.template.id && this.state.template.id > 0 ? 'Edit ' : 'New '
                                }
                                Template
                            </h3>
                            <TemplateForm
                                formState={this.state.formState}
                                categories={this.state.categories}
                                category={this.state.category}
                                template={this.state.template}
                                onChange={(e, template) => this.templateChangeHandler(e, template)} 
                                onSave={(e) => this.saveTemplateHandler(e)} 
                            />
                        </Modal>
                    ) : ''
                }
                {
                    this.state.templateOnPreview ?  
                    (
                        <Modal onClose={() => this.closeTemplatePreview()} size={ModalSizes.FILL}>
                            <TemplatePreview
                                template={this.state.templateOnPreview}
                            />
                        </Modal>
                    ) : ''
                }
                <div>
                    
                    <div className="container-fluid mb-3">
                        <div className="row">
                            <div className="col-12 d-flex align-items-center template-search">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Search..."
                                    value={this.state.searchTerm}
                                    onChange={(e) => this.templateSearch(e)} 
                                />
                                {
                                    this.state.searchTerm ? 
                                    (<span className="cursor-pointer clear-search" onClick={() => this.clearSearch()}>&times;</span>) :
                                    ''
                                }
                                
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid" >
                    {
                        this.state.searchResult ?
                        (
                            <div className="row">
                                <TemplateList 
                                    select={(e, t) => this.props.select(e, t)} 
                                    onEdit={(e, t) => this.editHandler(e, t)}
                                    onDelete={(e, t) => this.deleteHandler(e, t)}
                                    templates={this.state.searchResult}
                                    previewTemplate={(e, t) => this.previewTemplate(e, t)}
                                />
                            </div>
                        )
                        :
                        this.state.categories.map((category: ICategory) => (
                            <Aux key={category.id}>
                                {category.name ? 
                                    (
                                        <div className="row" >
                                            <div className="col-12 mb-3">
                                                <h3 className="border-bottom pb-1 d-flex justify-content-between">
                                                    <span>{category.name}</span>


                                                    {this.props.isPowerUser ? (
                                                        <button 
                                                            type="button" 
                                                            className="btn btn-sm btn-info ml-2"
                                                            onClick={(e) => this.editTemplate(e, {category: category.id,  id: -1})}
                                                        >
                                                            New Template
                                                        </button>
                                                        ) : ''
                                                    }
                                                </h3>
                                            </div>
                                        </div>
                                    ) : 
                                    ''
                                }

                                <div className="row">
                                    <TemplateList 
                                        select={(e, t) => this.props.select(e, t)} 
                                        onEdit={(e, t) => this.editHandler(e, t)}
                                        onDelete={(e, t) => this.deleteHandler(e, t)}
                                        templates={category.templates}
                                        previewTemplate={(e, t) => this.previewTemplate(e, t)}
                                    />
                                </div>
                            </Aux>
                        ))
                    }
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(mapStateToProps, null)(Templates);

