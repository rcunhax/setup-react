import * as React from 'react';
import { ITemplate, ICategory } from '../../model/models';
import FileUpload from '../ui/fileupload';
import { SERVER_FILES_URL } from '../../constants';

interface TemplateFormProps {
    category: ICategory;
    categories: ICategory[];
    onSave: (e: any) => void;
    template?: ITemplate;
    onChange: (e: any, template?: ITemplate) => void;
    formState: {
        uploadProgress?: number;
        saving?: boolean;
    };
}

const TemplateForm: React.SFC<TemplateFormProps> = (props) => {    
    
    const onFiles = (name: string, files: File[]) => {
        props.template[name] = files[0];        
        props.onChange(null, props.template);
    };

    const progressStyle = {
        width: props.formState.uploadProgress + '%'
    };

    return (
        <div className="position-relative">

            {props.formState.saving ? 
                (<div className="backdrop">
                    <div className="form-status">
                        <div className="progress">
                            <div className="progress-bar bg-warning" style={progressStyle}/>
                        </div>
                    </div>
                </div>)  : ''
            }
            
            <form onSubmit={(e) => props.onSave(e)}>
                <div className="form-group">
                    <label>Name</label>
                    <input
                        type="text"
                        name="name"
                        value={props.template.name}
                        className="form-control"
                        placeholder="Name"
                        onChange={(e) => props.onChange(e)} 
                    />
                </div>

                <div className="form-group">
                    <label>Description</label>
                    <textarea
                        name="description"
                        className="form-control"
                        value={props.template.description}
                        placeholder="Description"
                        onChange={(e) => props.onChange(e)} 
                    />
                </div>

                <div className="form-group">
                    <label >Category</label>
                    <select 
                        id="inputState"
                        onChange={(e) => props.onChange(e)} 
                        className="form-control"
                        value={props.template.category}
                        name="category"
                    >
                        <option defaultValue="">Choose...</option>
                        {props.categories.map((category: ICategory) => (
                            <option key={category.id} value={category.id}>{category.name}</option>)
                        )}
                    </select>
                </div>

                <div className="form-group">
                    <label>Demo URL</label>
                    <input
                        type="text"
                        name="demoUrl"
                        value={props.template.demoUrl}
                        className="form-control"
                        placeholder="Demo URL"
                        onChange={(e) => props.onChange(e)} 
                    />
                </div>

                <div className="form-group">
                    <div>
                        <label>File</label>
                        {
                            props.template.templateFileId ? 
                            (
                                <strong className="ml-2">{props.template.templateFileId}</strong>
                            )
                            :
                            ''
                        }
                    </div>
                    <FileUpload onFiles={(files) => onFiles('fileFile', files)} showPreview={false}>
                        <p>
                            Drag && Drop your template file (.lar)
                            <br/>
                            Or click bellow
                        </p>
                    </FileUpload>
                </div>
                <div className="form-group">

                    <div>
                        <label >Image</label>
                        {
                            props.template.templateImageId ? 
                            (
                                <img className="ml-2 template-small-image" src={`${SERVER_FILES_URL}/${props.template.templateImageId}`}/>
                            )
                            :
                            ''
                        }
                    </div>
                    
                    <FileUpload onFiles={(files) => onFiles('imageFile', files)} showPreview={true}>
                        <p>
                            Drag && Drop your image
                            <br/>
                            Or click bellow
                        </p>
                    </FileUpload>
                </div>

                <div className="form-group">
                    <input type="submit" className="btn btn-primary" value="Submit" />
                </div>
            </form>
        </div>
    );
};

export default TemplateForm;
