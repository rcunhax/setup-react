
import * as React from 'react';
import { IUser } from '../../model/models';
import { DbyHttp } from '../../http';

interface UserFormsState {
    user: IUser;
    errorMessage?: string;
}

interface UserFormsProps {
    onSave: (user: IUser) => void;
}

class UserForm extends React.Component<UserFormsProps> {

    state: UserFormsState = {
        user: {}
    };

    constructor(props: UserFormsProps) {
        super(props);
    }

    passwordChangeHandler(event: any) {
        const name = event.target.name;
        const value = event.target.value;
        const user = this.state.user;
        let passwodsMatch = false;
        if (name === 'password1' && user.password2) {
            passwodsMatch = user.password2 === value;
        } else if (this.state.user.password1) {
            passwodsMatch = user.password1 === value;
        }
        user[name] = value;
        this.setState({
            user: user,
            errorMessage: passwodsMatch ? null : 'Passwords don\'t match'
        });

    }

    changeHandler(event: any) {
        const name = event.target.name;
        const value = event.target.value;
        const user = this.state.user;
        user[name] = value;
        this.setState({
            user: user
        });
  
    }

    saveUser(event: any) {
        event.preventDefault();
        const user: IUser = this.state.user;
        user.autoScreenName = true;
        user.sendEmail  = true;
        user.password = user.password1;
        const formData = new FormData();
        formData.append('userJson', JSON.stringify(user));
        DbyHttp.doLiferayPost<IUser>('dby.dbysite/add-user', formData)
        .then(response => {
            console.log(response);
            this.props.onSave(response.data);
        }).catch(() => {
            this.setState({
                errorMessage: 'There was an error processing your request'
            });
        });
    }

    render() {
        return (
            <div className="position-relative">

            <form onSubmit={(e) => this.saveUser(e)}>

                <div className="form-group">
                    <label >Screen Name</label>
                    <input
                        type="text"
                        name="screenName"
                        value={this.state.user.screenName}
                        className="form-control"
                        placeholder="Screen Name"
                        onChange={(e) => this.changeHandler(e)} 
                    />
                </div>

                <div className="form-group">
                    <label >First Name</label>
                    <input
                        type="text"
                        name="firstName"
                        value={this.state.user.firstName}
                        className="form-control"
                        placeholder="Name"
                        onChange={(e) => this.changeHandler(e)} 
                    />
                </div>

                <div className="form-group">
                    <label>Last Name</label>
                    <input
                        type="text"
                        name="lastName"
                        value={this.state.user.lastName}
                        className="form-control"
                        placeholder="Name"
                        onChange={(e) => this.changeHandler(e)} 
                    />
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input
                        type="email"
                        name="emailAddress"
                        value={this.state.user.emailAddress}
                        className="form-control"
                        placeholder="Email"
                        onChange={(e) => this.changeHandler(e)} 
                    />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input
                        type="password"
                        name="password1"
                        value={this.state.user.password1}
                        className="form-control"
                        placeholder="Confirm Password"
                        onChange={(e) => this.passwordChangeHandler(e)} 
                    />
                </div>
                <div className="form-group">
                    <label>Confirm Password</label>
                    <input
                        type="password"
                        name="password2"
                        value={this.state.user.password2}
                        className="form-control"
                        placeholder="Confirm Password"
                        onChange={(e) => this.passwordChangeHandler(e)} 
                    />
                </div>

                {
                    this.state.errorMessage ? 
                    (
                        <p className="text-danger text-center">{this.state.errorMessage}</p>
                    )
                    :
                    ''
                }

                <div className="form-group">
                    <input type="submit" className="btn btn-primary" value="Submit" />
                </div>
            </form>
        </div>
        );
    }
}

export default UserForm;
