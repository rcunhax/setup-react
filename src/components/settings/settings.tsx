import * as React from 'react';
import { DbyHttp } from '../../http';
import { AxiosResponse } from 'axios';
import { SETTINGS_API_URLS } from '../../constants';
import { ISetting, ISettingValueType } from '../../model/models';


class Settings extends React.Component<any> {

    state: {settings: ISetting[], newSettings: ISetting[]} = {
        settings: [],
        newSettings: []
    };

    loaded: boolean;
    valueTypes: string[] = [];
    mandatorySettings: string[] = [];

    constructor(props: any) {
        super(props);
        this.valueTypes = Object.keys(ISettingValueType).map(key => ISettingValueType[key]);
    }

    componentDidMount() {
        this.loadMandatorySettings();
    }

    loadMandatorySettings() {
        DbyHttp.doGet(SETTINGS_API_URLS.MANDATORY_SETTINGS)
        .then((response: AxiosResponse<string[]>) => {
            this.loaded = true;
            this.mandatorySettings = response.data;
            this.loadSettings();
        });
    }

    loadSettings() {
        DbyHttp.doGet(SETTINGS_API_URLS.GET_ALL_SETTINGS)
        .then((response: AxiosResponse<ISetting[]>) => {
            const settings = response.data;
            const newSettings: ISetting[] = [];
            if (settings && settings.length) {
                 this.mandatorySettings.forEach(set => {
                    if (settings.findIndex(setting => setting.name === set) === -1) {
                        newSettings.push({
                            name: set,
                            id: (newSettings.length + 1) * -1,
                            valueType: ISettingValueType.STRING
                        });
                    }
                });
            } else {
                this.mandatorySettings.forEach(set => {
                    newSettings.push({
                        name: set,
                        id: (newSettings.length + 1) * -1,
                        valueType: ISettingValueType.STRING
                    });
                });
            }
           
            this.loaded = true;
            this.setState({
                settings: settings.map(this.setSettingValue),
                newSettings: newSettings
            });
        });
    }

    setSettingValue(setting: ISetting): ISetting {
        switch (setting.valueType) {
            case ISettingValueType.BOOLEAN:
                setting.value = '' + setting.booleanValue;
                break;
            case ISettingValueType.DATE:
                setting.value = '' + setting.dateValue;
                break;
            case ISettingValueType.INT:
                setting.value = '' + setting.intValue;
                break;
            case ISettingValueType.LONG:
                setting.value = '' + setting.longValue;
                break;
            case ISettingValueType.DOUBLE:
                setting.value = '' + setting.doubleValue;
                break;   
            default:
                setting.value = setting.stringValue;
        }
        return setting;
    }

    setSettingValueType(setting: ISetting): ISetting {
        switch (setting.valueType) {
            case ISettingValueType.BOOLEAN:
                setting.booleanValue = Boolean(setting.value);
                break;
            case ISettingValueType.DATE:
                setting.dateValue = new Date(setting.value);
                break;
            case ISettingValueType.INT:
                setting.intValue = +setting.value;
                break;
            case ISettingValueType.LONG:
                setting.longValue = +setting.value;
                break;
            case ISettingValueType.DOUBLE:
                setting.doubleValue = +setting.value;
                break;   
            default:
                setting.stringValue = setting.value;
        }
        return setting;
    }
    
    saveNewSettings(event: any) {
        event.preventDefault();
        const newSettings = this.state.newSettings
            .filter(set => typeof set.value !== 'undefined');
        
        if (newSettings && newSettings.length) {

            newSettings.forEach(setting => {
                setting = this.setSettingValueType(setting);
                delete setting.value; 
            });
            const formData = new FormData();
            formData.append('dbySettingsJson', JSON.stringify(newSettings));
            DbyHttp.doLiferayPost<ISetting[]>('dby.dbysettings/save-settings', formData)
            .then(response => {
                console.log(response);
                this.loadSettings();
            }).catch(err => {
                console.log(err);
            });
        }
    }

    saveSetting(setting: ISetting, index: number) {
        const formData = new FormData();
        setting = this.setSettingValueType(setting);
        formData.append('dbySettingsJson', JSON.stringify(setting));
        DbyHttp.doLiferayPost<ISetting>('dby.dbysettings/save-setting', formData)
        .then(response => {
            console.log(response);
            this.loadSettings();
        }).catch(err => {
            console.log(err);
        });
    }

    newSetting() {
        const newSettings = this.state.newSettings || [];
        newSettings.push({
            id: (newSettings.length + 1) * -1,
            valueType: ISettingValueType.STRING
        });
        this.setState({
            newSettings: newSettings
        });
    }

    editSetting(setting: ISetting, index: number, field: string) {
        const settings = this.state.settings;
        setting[field] = true;

        settings.splice(index, 1, setting);

        this.setState({
            settings: settings
        });
    }

    cancelEdit(setting: ISetting, index: number, field: string) {
        const settings = this.state.settings;
        setting[field] = false;

        settings.splice(index, 1, setting);

        this.setState({
            settings: settings
        });
    }

    deleteSetting(setting: ISetting, index: number) {
        DbyHttp.doDelete(`${SETTINGS_API_URLS.DELETE_SETTING}?id=${setting.id}`)
        .then(() => {
            const settings = this.state.settings;
            settings.splice(index, 1);
            this.setState({
                settings: settings
            });
        });
    }

    removeSetting(index: number) {
        const newSettings = this.state.newSettings || [];
        newSettings.splice(index, 1);
        this.setState({
            newSettings: newSettings
        });
    }

    onSettingChange(event: any, setting: ISetting, field: string) {
        const settings = this.state.settings || [];
        const index = settings.findIndex(set => set.id === setting.id);
        let value = event.target.value;

        if (event.target.type === 'checkbox') {
            value = event.target.checked;
        }

        if (index > -1) {
            setting[field] = value;
            settings.splice(index, 1, setting);
        }
        this.setState({
            settings: settings
        });
    }

    onNewSettingChange(event: any, setting: ISetting, field: string) {
        const newSettings = this.state.newSettings || [];
        const index = newSettings.findIndex(set => set.id === setting.id);
        let value = event.target.value;

        if (event.target.type === 'checkbox') {
            value = event.target.checked;
        }

        if (index > -1) {
            setting[field] = value;
            newSettings.splice(index, 1, setting);
        }
        this.setState({
            newSettings: newSettings
        });
    }


    getNewSettingInput(setting: ISetting): any {
        return this.getSettingInput(setting, (e, s, f) => this.onNewSettingChange(e, s, f));
    }

    getSettingInput(setting: ISetting, changeFunction?: (event: any, setting: ISetting, field: string) => void): any {
        changeFunction = changeFunction ? changeFunction : (e, s, f) => this.onSettingChange(e, s, f);
        let input;
        let value = setting.value;
        switch (setting.valueType) {
            case ISettingValueType.BOOLEAN:
                input = (
                    <input
                        type="checkbox"
                        value={value}
                        className="ml-1"
                        placeholder="Value"
                        onChange={(e) => changeFunction(e, setting, 'value')}
                    />
                    );
                break;
            case ISettingValueType.DATE:
                input = (
                    <input
                        type="text"
                        value={value}
                        className="form-control ml-1"
                        placeholder="Value"
                        onChange={(e) => changeFunction(e, setting, 'value')}
                    />
                    );
                break;
            case ISettingValueType.INT:
                input = (
                    <input
                        type="text"
                        value={value}
                        className="form-control ml-1"
                        placeholder="Value"
                        onChange={(e) => changeFunction(e, setting, 'value')}
                    />
                    );
                break;
            case ISettingValueType.LONG:
                input = (
                    <input
                        type="number"
                        value={value}
                        className="form-control ml-1"
                        placeholder="Value"
                        onChange={(e) => changeFunction(e, setting, 'value')}
                    />
                    );
                break;
            case ISettingValueType.DOUBLE:
                input = (
                    <input
                        type="number"
                        value={value}
                        className="form-control ml-1"
                        placeholder="Value"
                        onChange={(e) => changeFunction(e, setting, 'value')}
                    />
                    );
                break;   
            default:
                input = (
                    <input
                        type="text"
                        value={value}
                        className="form-control ml-1"
                        placeholder="Value"
                        onChange={(e) => changeFunction(e, setting, 'value')}
                    />
                    );
        }
        return input;
    }

    render() {
        return (
            <div>
                <h3>Settings</h3>
                {
                    this.state.settings.length ?

                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Value Type</th>
                                <th scope="col">Value</th>
                                <th scope="col"/>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.settings.map((setting, index) => (
                                <tr key={setting.id}>
                                    <td>
                                    {
                                            setting.editingName ? 
                                            (
                                                <div className="d-flex align-items-center">
                                                    <input
                                                        type="text"
                                                        value={setting.name}
                                                        className="form-control"
                                                        placeholder="Name"
                                                        onChange={(e) => this.onSettingChange(e, setting, 'name')} 
                                                    />
                                                    <button 
                                                        type="button" 
                                                        title="Save"  
                                                        className="btn btn-sm btn-primary ml-1" 
                                                        onClick={() => this.saveSetting(setting, index)}
                                                    >
                                                        <i 
                                                            className="fa fa-check" 
                                                        />
                                                    </button>
                                                    <button 
                                                        type="button" 
                                                        title="Cancel"  
                                                        className="btn btn-sm btn-default ml-1" 
                                                        onClick={() => this.cancelEdit(setting, index, 'editingName')}
                                                    >
                                                        cancel
                                                    </button>
                                                </div>
                                            )
                                            : 
                                            (
                                                <span onDoubleClick={() => this.editSetting(setting, index, 'editingName')}>{setting.name}</span>
                                            ) 
                                        }
                                    </td>

                                    
                                    <td>{setting.valueType}</td>
                                    <td>
                                        {
                                            setting.editingValue ? 
                                            (
                                                <div className="d-flex align-items-center">
                                                    {this.getSettingInput(setting)}
                                                    <button 
                                                        type="button" 
                                                        title="Save"  
                                                        className="btn btn-sm btn-primary ml-1" 
                                                        onClick={() => this.saveSetting(setting, index)}
                                                    >
                                                        <i 
                                                            className="fa fa-check" 
                                                        />
                                                    </button>
                                                    <button 
                                                        type="button" 
                                                        title="Cancel"  
                                                        className="btn btn-sm btn-default ml-1" 
                                                        onClick={() => this.cancelEdit(setting, index, 'editingValue')}
                                                    >
                                                        cancel
                                                    </button>
                                                </div>
                                            )
                                            : 
                                            (
                                                <span onDoubleClick={() => this.editSetting(setting, index, 'editingValue')}>{setting.value}</span>
                                            ) 
                                        }
                                    </td>
                                    <td>
                                        <button type="button" className="btn btn-sm btn-danger" onClick={() => this.deleteSetting(setting, index)}>
                                            <i className="fa fa-trash" aria-hidden="true" />
                                        </button>
                                    </td>
                                </tr>
                            ))}                            
                        </tbody>
                    </table>
                    :
                        (
                            <div>
                                <p className="mb-0">There are no settings</p>
                            </div>
                        )
                }
                <button type="button" className="btn btn-sm btn-info" onClick={() => this.newSetting()}>New</button>
                {
                    
                    this.state.newSettings.length ?
                        <div className="mt-2">
                            <h5><strong>New Settings</strong></h5>
                            <button type="button" className="btn btn-sm mb-2"  onClick={() => this.newSetting()}>
                                <i className="fa fa-plus" aria-hidden="true"/>
                            </button>
                            <form onSubmit={(e) => this.saveNewSettings(e)}>
                                {
                                    this.state.newSettings.map((setting, index) => (
                                    <div key={setting.id} className="setting">
                                        <div className="form-group">

                                            <div className="d-flex justify-content-center align-items-center">

                                                <input
                                                    type="text"
                                                    value={setting.name}
                                                    className="form-control"
                                                    placeholder="Name"
                                                    onChange={(e) => this.onNewSettingChange(e, setting, 'name')} 
                                                />

                                                <select 
                                                    onChange={(e) => this.onNewSettingChange(e, setting, 'valueType')} 
                                                    className="form-control ml-1"
                                                    value={setting.valueType}
                                                    name="category"
                                                >
                                                    <option defaultValue="">Choose...</option>
                                                    {this.valueTypes.map(valueType => (
                                                        <option key={valueType} value={valueType}>{valueType}</option>)
                                                    )}
                                                </select>

                                                {this.getNewSettingInput(setting)}
                                            

                                                <button type="button" className="btn btn-sm btn-warning ml-1"  onClick={() => this.removeSetting(index)}>
                                                    <i className="fa fa-minus" aria-hidden="true"/>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    ))
                                }
                                <div className="form-group">
                                    <button type="submit" className="btn btn-sm btn-primary">Save</button>
                                </div>

                            </form>
                        </div>
                    : ''
                }

            </div>
        );
    }
}

export default Settings;
