import * as React from 'react';
import { AxiosResponse } from 'axios';
import { IAppStateProps, ICategory } from '../../model/models';
import { CATEGORY_API_URLS, mapStateToProps } from '../../constants';
import CategoryForm from './category-form';
import { DbyHttp } from '../../http';
import Modal from '../hoc/modal';
import { connect } from 'react-redux';
import Aux from '../hoc/auxiliar';

interface ICategoriesProps  {
    categories?: ICategory[];
    categorySelect?: (cat: ICategory) => void;
}

type AllProps = ICategoriesProps & IAppStateProps;


class Categories extends React.Component<AllProps> {

    state: {categories: ICategory[], category?: ICategory, toDeleteCategory?: ICategory} = {
        categories: []
    };

    constructor(props: ICategoriesProps) {
        super(props);
    }

    componentDidMount() {
        if (!this.props.categories || !this.props.categories.length) {
           this.getAllCategories(); 
        } else {
            this.setState({
                categories: this.props.categories,
                category: null,
                toDeleteCategory: null
            });
        }
    }

    closeCategoryForm() {        
        this.setState({
            category: null
        });
    }

    saveCategory(event: any) {
        event.preventDefault();
        const category = this.state.category || {};
        if (!category.id) {
            category.id = '-1';
        }

        const formData = new FormData();
        formData.append('categoryJson', JSON.stringify(category));
        DbyHttp.doLiferayPost('dby.dbycategory/save-category', formData)
        .then(response => {
            this.getAllCategories();
        });
    }

    getAllCategories() {
        DbyHttp.doGet(CATEGORY_API_URLS.GET_ALL_CATEGORIES)
        .then((response: AxiosResponse<ICategory[]>) => {
            this.setState({
                categories: response.data,
                category: null,
                toDeleteCategory: null
            });
        });
    }
    
    changeCategoryHandler(event: any) {
        const name = event.target.name;
        const value = event.target.value;
        const category = this.state.category || {};
        category[name] = value;
        this.editHandler(event, category);
    }

    editHandler(event: any, category: ICategory) {
        this.setState({
            category: category
        });
    }

    deleteHandler (event: any, category: ICategory) {
        this.setState({
            toDeleteCategory: category
        });
    }

    doDelete() {
        DbyHttp.doDelete(`${CATEGORY_API_URLS.DELETE_CATEGORY}?id=${this.state.toDeleteCategory.id}`)
        .then(() => {
            this.getAllCategories();
        });
    }

    categorySelect(event: any, category: ICategory) {
        if (this.props.categorySelect) {
            this.props.categorySelect(category);
        }
    }
    
    render() {
        return (
            <div className="category-list">
                <div>
                    <h3 className="category-list-title mb-4">
                        Categories
                    </h3>
                    <div className="mb-5">
                        {this.state.categories.map((category: ICategory) => (
                            <div className="d-flex align-items-center mb-3 category line-height-1" data-id={category.id} key={category.id}>
                                <a 
                                    className="mr-2 cursor-pointer text-muted line-height-1" 
                                    onClick={(e) => this.categorySelect(e, category)}
                                >
                                    {category.name}
                                </a>

                                {this.props.isPowerUser ? (
                                    <div className="edit-icons">
                                        <i 
                                            className="fa fa-pencil cursor-pointer mx-1" 
                                            onClick={(e) => this.editHandler(e, category)}
                                        />
                                        <i
                                            className="fa fa-trash cursor-pointer mx-1"
                                            onClick={(e) => this.deleteHandler(e, category)}
                                        />
                                    </div>
                                    ) : ''
                                }

                                
                            </div>
                        ))}
                    </div>
                    
                    {this.props.isPowerUser ? (
                        <button 
                            type="button" 
                            className="btn btn-sm btn-block btn-info"
                            onClick={(e) => this.editHandler(e, {id: -1})}
                        >
                        New
                        </button>) : ''
                    }
                </div>

                {this.props.isPowerUser ? (
                    <Aux>
                        {
                            this.state.category ?  
                            (
                            <Modal onClose={() => this.closeCategoryForm()}>
                                <CategoryForm 
                                    category={this.state.category} 
                                    onChange={(e) => this.changeCategoryHandler(e)} 
                                    onSave={(e) => this.saveCategory(e)} 
                                />
                            </Modal>
                            ) : ''
                        }
                        {
                            this.state.toDeleteCategory ? (
                                <Modal onClose={(e) => this.deleteHandler(e, null)}>
                                    <div>
                                        <h5 className="text-center">
                                            <div>
                                                Are you sure you want to delete
                                            </div>    
                                            <strong>
                                                '{this.state.toDeleteCategory.name}'
                                            </strong>
                                        </h5>
                                        <div className="text-center border-top pt-3">
                                            <button 
                                                type="button" 
                                                className="btn btn-info"
                                                onClick={() => this.doDelete()}
                                            >
                                                Yes
                                            </button>
                                            <button 
                                                type="button" 
                                                className="btn ml-2"
                                                onClick={(e) => this.deleteHandler(e, null)}
                                            >
                                                No
                                            </button>
                                        </div>
                                    </div>
                                </Modal>
                            ) : ''
                        }
                    </Aux> 
                    )
                    : ''
                }

            </div>
        );
    }
}

export default connect(mapStateToProps, null)(Categories);
