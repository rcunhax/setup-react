import * as React from 'react';
import { ICategory } from '../../model/models';

interface CategoryFormProps {
    category: ICategory;
    onSave: (e: any) => void;
    onChange: (e: any) => void;
}

const CategoryForm: React.SFC<CategoryFormProps> = (props) => {       
    return (props.category ? (
        <form onSubmit={(e) => props.onSave(e)}>
            <div className="form-group">
                <label >Name</label>
                <input
                    type="text"
                    name="name"
                    value={props.category.name}
                    className="form-control"
                    placeholder="Name"
                    onChange={(e) => props.onChange(e)} 
                />
            </div>
            <div className="form-group">
                <label>Description</label>
                <textarea
                    name="description"
                    className="form-control"
                    value={props.category.description}
                    placeholder="Description"
                    onChange={(e) =>  props.onChange(e)} 
                />
            </div>
            <div className="form-group">
                <input type="submit" className="btn btn-primary" value="Submit" />
            </div>
        </form>
    ) : (<div>Loading</div>));
};

export default CategoryForm;
