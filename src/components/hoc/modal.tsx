import * as React from 'react';
import Aux from './auxiliar';


export enum ModalSizes {
    LARGE = 'modal-lg',
    SMALL = 'modal-sm',
    AUTO = '',
    FILL = 'modal-fill'
}

interface ModalProps {
    onClose: () => void;
    children?: React.ReactNode;
    size?: ModalSizes;
}

const Modal: React.SFC<any> = (props: ModalProps) => {
    return  (
        <Aux>
            <div className="modal fade show">
                <div className={`modal-dialog ${props.size ? props.size : ModalSizes.LARGE}`}>
                    <div className="modal-content">
                        <span  className="close" onClick={props.onClose} role="button" aria-pressed="false">×</span>
                        <div className="modal-body">
                            {props.children}
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal-backdrop fade show" />
        </Aux>
    );
};
export default Modal;