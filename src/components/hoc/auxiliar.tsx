import * as React from 'react';

const Aux: React.SFC<any> = (props) => props.children;
export default Aux;