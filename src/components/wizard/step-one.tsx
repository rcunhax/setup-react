import * as React from 'react';
import Templates from '../templates/templates';
import { AxiosResponse } from 'axios';
import { CATEGORY_API_URLS } from '../../constants';
import { ICategory, ITemplate, IWizardStep } from '../../model/models';
import { DbyHttp } from '../../http';
import Categories from '../category/categories';
import Aux from '../hoc/auxiliar';

export class StepOne extends React.Component<IWizardStep> {

    state: {categories?: ICategory[], template?: ITemplate, category?: ICategory} = {};
    loaded: boolean;
    constructor(props: IWizardStep) {
        super(props);
    }

    componentDidMount() {
        this.getAllCategories();
    }

    getAllCategories() {
        DbyHttp.doGet(CATEGORY_API_URLS.GET_ALL_CATEGORIES)
        .then((response: AxiosResponse<ICategory[]>) => {
            this.loaded = true;
            this.setState({
                categories: response.data
            });
        });
    }

    onCategorySelect(category: ICategory) {
        this.setState({
            category: category
        });
    }

    onTemplateSelect(template: ITemplate) {        
        this.props.next(template);
    }

    render() {
        return (
            <div className="template-select mx-5 mb-5">
                <div className="container-fluid">
                    {
                        this.loaded ? 
                        (
                            <Aux>
                                <div className="row">
                                    <div className="col-12">
                                        <h1 className="text-center mb-5 pb-3 border-bottom">Templates</h1>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-2">
                                        <Categories 
                                            categorySelect={(cat) => this.onCategorySelect(cat)} 
                                            categories={this.state.categories} 
                                        />
                                    </div>                    
                                    <div className="col-md-10">
                                        <Templates 
                                            category={this.state.category}  
                                            categories={this.state.categories} 
                                            select={(e, t) => this.onTemplateSelect(t)}
                                        />
                                    </div>
                                </div>
                            </Aux>
                        ) : 
                        (
                            <h3 className="text-center">Loading</h3>
                        )
                    }
                </div>
            </div>
        );
    }
}