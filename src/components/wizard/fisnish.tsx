import * as React from 'react';
import { ICategory, ITemplate, IWizardStep, IJob } from '../../model/models';


export class Finish extends React.Component<IWizardStep> {

    state: {
        categories?: ICategory[], 
        template?: ITemplate,
        job?: IJob
    } = {};

    timeout: any;

    constructor(props: IWizardStep) {
        super(props);
        this.setState({
            job: this.props.data.job
        });
    }

    componentDidMount() {
        this.timeout = setTimeout(() => this.getJobStatus(), 3000);
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    getJobStatus() {

    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="text-center">
                            <h1 className="mb-4">Templates</h1>
                            <img className="mb-4" src="/imgs/loading-stopwatch.svg"/>
                            <h4>This may take a few minutes...</h4>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}