import * as React from 'react';
import { ICategory, ITemplate, IWizardStep } from '../../model/models';
import { SiteDetails } from '../site-details';
import Template from '../templates/template';

export class StepTwo extends React.Component<IWizardStep> {

    state: {categories?: ICategory[], template?: ITemplate} = {};

    constructor(props: IWizardStep) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h1 className="text-center mb-5 pb-3 border-bottom">Site Details</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="px-4 mb-5">
                            <h3>Template</h3>
                            <Template template={this.props.data.template} displayOnly={true}/>
                        </div>
                    </div>
                    <div className="col-md-8">
                        <div className="px-4 mb-5 site-form-wrapper">
                            
                            <SiteDetails 
                                back={(e) => this.props.prev(e)} 
                                save={(site, logo) => this.props.next({site: site, logo: logo})}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}