import * as React from 'react';
import { StepOne } from './wizard/step-one';
import { StepTwo } from './wizard/step-two';
import { Action, ActionType, IAppStateProps, ISite, ITemplate, IUser, IJob } from '../model/models';
import { Finish } from './wizard/fisnish';
import { DbyHttp } from '../http';
import UserForm from './user/user.form';
import Modal, { ModalSizes } from './hoc/modal';
import Settings from './settings/settings';
import SitesManager from './admin/site-manager';
import { connect, Dispatch } from 'react-redux';
import { mapStateToProps, SITE_API_URLS } from '../constants';
import { AxiosResponse } from 'axios';
import Aux from './hoc/auxiliar';

interface IDispatchProps {
    setUser: (data: IAppStateProps) => void;
}

interface IAppProps extends IAppStateProps, IDispatchProps {}

const mapDispatchToProps = (dispatch: Dispatch<Action<IAppStateProps>>): IDispatchProps => {
    return {
        setUser: (data: IAppStateProps) => dispatch({type: ActionType.SET_USER, payload: data})
    };
};

interface IAppState {
    step?: number;
    editUser?: boolean;
    showSettings?: boolean;
    showSitesManager?: boolean;
}

class App extends React.Component<IAppProps, IAppState> {

    state: IAppState = {
        editUser: false,
        showSettings: false,
        showSitesManager: false
    };
    data: {
        template?: ITemplate;
        site?: ISite;
        logo?: File;
        job?: IJob;
    } = {};

    componentDidMount() {
        this.getUser();
        this.setStep(1);
    }

    nextStep(data: any) {
        if (this.state.step === 1) {
            this.data.template = data;
            this.setStep(++this.state.step); 
        } else if (this.state.step === 2) {
            const site = data.site as ISite;
            const template = this.data.template as ITemplate;
            site.category = template.category;
            site.templateId = template.id;
            site.id = -1;
            this.data.site = data.site;
            this.data.logo = data.logo;
            
            this.saveSite();
        } else {
            this.setStep(++this.state.step); 
        }
    }

    saveSite() {
        const formData = new FormData();
        formData.append('dbySiteJson', JSON.stringify(this.data.site));
        let url = 'dby.dbysite/add-site-no-logo';
        if (this.data.logo) {
            formData.append('logo', this.data.logo);
            url = 'dby.dbysite/add-site';
        }
        
        DbyHttp.doLiferayPost<IJob>(url, formData)
        .then(response => {
            this.data.job = response.data;
            this.setStep(++this.state.step);
        });
    }

    previousStep(data: any) {
        this.setStep(--this.state.step);     
    }

    setStep(step: number) {
        this.setState({
            step: step
        });

    }

    closeSettings() {
        this.setState({
            showSettings: false
        });
    }

    showSettings() {
        this.setState({
            showSettings: true
        });
    }

    closeSitesManager() {
        this.setState({
            showSitesManager: false
        });
    }
    showSitesManager() {
        this.setState({
            showSitesManager: true
        });
    }

    onNewUser(user: IUser) {
        this.closeUserModal();
    }

    closeUserModal() {
        this.setState({
            editUser: false
        });
    }

    getUser() {
        DbyHttp.doGet(SITE_API_URLS.GET_USER)
            .then((response: AxiosResponse<IUser>) => {                
                this.props.setUser({
                    isPowerUser: true,
                    user: response.data,
                    isLoggedIn: true,
                });
            });
    }

    render() {        
        let step;
        if (this.state.step === 1) {
            step = <StepOne next={(data) => this.nextStep(data)} prev={(data) => this.previousStep(data)} />;
        } else if (this.state.step === 2) {
            step = <StepTwo data={this.data} next={(data) => this.nextStep(data)} prev={(data) => this.previousStep(data)} />;
        } else {
            step = <Finish data={this.data}/>;
        }
        return (
            <div id="setup-page-wrap">
                {this.state.editUser ?
                    (
                        <Modal onClose={() => this.closeUserModal()}>
                            <UserForm onSave={(user) => this.onNewUser(user)} />
                        </Modal>
                    )
                    :
                    ''
                }
                {this.state.showSettings ?
                    (
                        <Modal size={ModalSizes.FILL} onClose={() => this.closeSettings()}>
                            <Settings />
                        </Modal>
                    )
                    :
                    ''
                }
                {this.state.showSitesManager ?
                    (
                        <Modal size={ModalSizes.FILL} onClose={() => this.closeSitesManager()}>
                            <SitesManager/>
                        </Modal>
                    )
                    :
                    ''
                }

                {step}

                <div className="footer text-center">
                    <div className="d-flex justify-content-center">
                        {this.props.isPowerUser ? 
                            <Aux>
                                <a href="javascript:;" className="text-muted mx-2" onClick={() => this.showSettings()}>Settings</a>
                                <a href="javascript:;" className="text-muted mx-2" onClick={() => this.showSitesManager()}>Sites Manager</a>
                            </Aux> : ''
                        }
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(App as any);
