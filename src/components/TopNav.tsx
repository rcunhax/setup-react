import * as React from 'react';

class TopNav extends React.Component<{}> {

    render() {
        return (
            <div className="top-nav">
                <nav className="nav">
                    <a className="nav-link active" href="#">Active</a>
                    <a className="nav-link" href="#">Link</a>
                    <a className="nav-link" href="#">Link</a>
                    <a className="nav-link disabled" href="#">Disabled</a>
                </nav>
            </div>
        );
    }
}

export default TopNav;
