import * as React from 'react';
import { ITemplate, ISite } from '../../model/models';
import { DbyHttp } from '../../http';
import { SITE_API_URLS } from '../../constants';
import FileUpload from '../ui/fileupload';

interface ISiteProps {
    template?: ITemplate; 
    site?: ISite;
    back: (e: any) => void;
    save: (site: ISite, logo: File) => void;
}

export class SiteDetails extends React.Component<ISiteProps> {

    state: {site: ISite, template?: ITemplate; isHostValid: boolean} = {
        site: {
            isUsingSharedHost: false,
            active: true
        },
        isHostValid: true,
    };

    logoFile: File;
    idValidationPattern = '[0-9a-zA-Z_.-]*';

    constructor(props: ISiteProps) {
        super(props);
    }

    componentDidMount() {
        if (this.props.site) {
            this.setState({
                site: this.props.site
            });
        }
        
    }
    
    saveHandler(event: React.FormEvent) {
        event.preventDefault();
        this.props.save(this.state.site, this.logoFile);
    }

    hostChangeHandler(event: any) {
        const host = event.target.value;
        let site = this.state.site || {};
        site.host = host;
        this.setState({
            site: site,
        });
        DbyHttp.doGet(SITE_API_URLS.IS_HOST_AVAILABLE + '?host=' + host)
        .then((response) => {
            this.setState({
                isHostValid: response.data === 'true'
            });
        });
    }

    changeUsingSharedHostHandler(event: any) {
        let site = this.state.site || {};
        site.isUsingSharedHost = !site.isUsingSharedHost;
        this.setState({
            site: site,
        });
    }

    changeHandler(event: any) {
        const name = event.target.name;
        const value = event.target.value;
        const site = this.state.site || {};

        site[name] = value;
        this.setState({
            site: site
        });
    }

    onFiles(files: File[]) {
        this.logoFile = files[0];        
    }

    render() {
        return (
            <div>
                <form onSubmit={(e) => this.saveHandler(e)}>
                    <div className="form-group">
                        <label>Name</label>
                        <input
                            type="text"
                            name="name"
                            value={this.state.site.name}
                            className="form-control"
                            placeholder="Name"
                            onChange={(e) => this.changeHandler(e)} 
                        />
                    </div>

                    <div className="form-group">
                        <div className="form-check">
                            <input 
                                className="form-check-input" 
                                onChange={(e) => this.changeUsingSharedHostHandler(e)}  
                                type="checkbox" 
                                name="isUsingSharedHost"
                                checked={this.state.site.isUsingSharedHost}
                            />
                            <label className="form-check-label">
                                Using own host
                            </label>
                        </div>
                    </div>

                    <div className="form-group">
                        <label>Host</label>
                        {
                            this.state.site.isUsingSharedHost ? (
                                <input
                                    type="text"
                                    name="name"
                                    value={this.state.site.host}
                                    className="form-control"
                                    placeholder="Name"
                                    onChange={(e) => this.hostChangeHandler(e)} 
                                />
                            ) : (
                                <div className="input-group">
                                    <input
                                        type="text"
                                        name="name"
                                        value={this.state.site.host}
                                        className="form-control"
                                        placeholder="Name"
                                        onChange={(e) => this.hostChangeHandler(e)} 
                                    />
                                    <div className="input-group-append">
                                        <span className="input-group-text">
                                            <strong>
                                                .pamodi.com
                                            </strong>
                                        </span>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                    <div className="form-group">
                        <label>Description</label>
                        <textarea
                            name="description"
                            className="form-control"
                            value={this.state.site.description}
                            placeholder="Description"
                            onChange={(e) => this.changeHandler(e)} 
                        />
                    </div>

                    <div className="form-group">
                        <label>Logo</label>
                        <FileUpload onFiles={(files) => this.onFiles(files)} showPreview={true}>
                            <p>
                                Drag && Drop your logo
                                <br/>
                                Or click bellow
                            </p>
                        </FileUpload>
                    </div>

                    <div className="form-group">
                        <button type="submit" className="btn btn-primary" >Submit</button>
                        <button type="button" onClick={this.props.back} className="btn btn-default ml-2">Back</button>

                    </div>
                </form>
            </div>
        );
    }
}
