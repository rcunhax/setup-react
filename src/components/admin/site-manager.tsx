import * as React from 'react';
import { DbyHttp } from '../../http';
import { AxiosResponse } from 'axios';
import { SITE_API_URLS } from '../../constants';
import { ISite } from '../../model/models';
import Aux from '../hoc/auxiliar';


class SitesManager extends React.Component<any> {

    state: {sites: ISite[], site?: ISite} = {
        sites: []
    };

    loaded: boolean;

    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        this.loadSites();
    }

    loadSites() {
        DbyHttp.doGet(SITE_API_URLS.GET_ALL_SITES)
        .then((response: AxiosResponse<ISite[]>) => {
            const sites = response.data;
            this.loaded = true;
            this.setState({
                sites: sites
            });
        });
    }

    deleteSite(site: ISite, index: number) {
        DbyHttp.doDelete(`${SITE_API_URLS.DELETE_SITE}?id=${site.id}`)
        .then(() => {
            this.loadSites();
        });
    }
    
    editSite(site: ISite, index: number) {

    }

    render() {

        return (
            <Aux>
            {
                (this.state.sites && this.state.sites.length) ?
                    (
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Host</th>
                                <th scope="col">Description</th>
                                <th scope="col">Owner</th>
                                <th scope="col">TemplateId</th>
                                <th scope="col">Active</th>
                                <th scope="col">Created</th>
                                <th scope="col">Modified</th>
                                <th scope="col"/>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.sites.map((site: ISite, index: number) => (
                                <tr key={site.id}>
                                    <td>{site.id}</td>
                                    <td>{site.name}</td>
                                    <td>{site.host}</td>
                                    <td>{site.description}</td>
                                    <td>{site.owner}</td>
                                    <td>{site.templateId}</td>
                                    <td>{site.active}</td>
                                    <td>{site.createDate}</td>
                                    <td>{site.modifiedDate}</td>
                                    <td>
                                        <button type="button" className="btn btn-sm btn-danger" onClick={() => this.deleteSite(site, index)}>
                                            <i className="fa fa-trash" aria-hidden="true" />
                                        </button>
                                        <button type="button" className="btn btn-sm btn-warning ml-1" onClick={() => this.editSite(site, index)}>
                                            <i className="fa fa-pencil" aria-hidden="true" />
                                        </button>
                                    </td>
                                </tr>
                            ))
                        }                    
                        </tbody>
                    </table>
                    )
                    :
                    <h6 className="text-muted text-center col-12 mb-4">
                        No Sites
                    </h6>
            }
        </Aux>
            
        );
        
    }
}

export default SitesManager;
