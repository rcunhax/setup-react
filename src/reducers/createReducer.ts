import { Action } from '../model/models';

export default function createReducer<T>(initialState: T, handlers: Object) {
    return function reducer(state: Object = initialState, action: Action<T>) {
        if (handlers.hasOwnProperty(action.type)) {
            return handlers[action.type](state, action);
        } else {
            return state;
        }
    };
}