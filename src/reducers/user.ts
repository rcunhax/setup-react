import createReducer from './createReducer';
import { Action, ActionType, IAppStateProps } from '../model/models';

// const initialState: TeamsState = {
//     data: [],
//     errors: undefined,
//     selected: undefined,
//     loading: false
//   }

export const user = createReducer({}, {
    [ActionType.SET_USER](state: IAppStateProps, action: Action<IAppStateProps>) {
        console.log(state);
        return action.payload;
    }
});