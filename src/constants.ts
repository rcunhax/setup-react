import { IAppStateProps } from './model/models';

export const SERVER_URL = 'http://localhost:8080';

export const SERVER_FILES_URL = SERVER_URL + '/o/dby/file';

export const SITE_API_URL = SERVER_URL + '/api/jsonws/dby.dbysite';

export const SITE_API_URLS = {
    GET_AVAILABLE_THEMES: SITE_API_URL + '/get-available-themes',
    CREATE_SITE: SITE_API_URL + '/create-site',
    IS_HOST_AVAILABLE: SITE_API_URL + '/is-host-available',
    ADD_SITE_LOGO: SITE_API_URL + '/add-site-logo',
    SAVE_SITE: SITE_API_URL + '/add-site',
    SAVE_SITE_NO_LOGO: SITE_API_URL + 'add-site-no-logo',
    GET_ALL_SITES: SITE_API_URL + '/get-all-sites',
    DELETE_SITE: SITE_API_URL + '/delete-site',
    GET_USER: SITE_API_URL + '/get-logged-in-user',
    ADD_USER: SITE_API_URL + '/add-user'
};

export const CATEGORY_API_URL = SERVER_URL + '/api/jsonws/dby.dbycategory';
export const CATEGORY_API_URLS = {
    SAVE_CATEGORY: CATEGORY_API_URL + '/save-category',
    GET_ALL_CATEGORIES: CATEGORY_API_URL + '/get-all-categories',
    DELETE_CATEGORY: CATEGORY_API_URL + '/delete-category'
};
export const TEMPLATE_API_URL = SERVER_URL + '/api/jsonws/dby.dbysitetemplate';
export const TEMPLATE_API_URLS = {
    SAVE_TEMPLATE: TEMPLATE_API_URL + '/save-site-template-json',
    GET_ALL_TEMPLATES: TEMPLATE_API_URL + '/get-all-site-templates',
    GET_TEMPLATES: TEMPLATE_API_URL + '/get-site-templates',
    DELETE_TEMPLATE: TEMPLATE_API_URL + '/delete-site-template',
    DELETE_ALL_TEMPLATES: TEMPLATE_API_URL + '/delete-all-templates',
    FIND_BY_NAME: TEMPLATE_API_URL + '/find-by-name-starting-with'
};

export const SETTINGS_API_URL = SERVER_URL + '/api/jsonws/dby.dbysettings';
export const SETTINGS_API_URLS = {
    SAVE_SETTING: SETTINGS_API_URL + '/save-setting',
    SAVE_SETTINGS: SETTINGS_API_URL + '/save-settings',
    MANDATORY_SETTINGS: SETTINGS_API_URL + '/get-mandatory-settings-names',
    GET_ALL_SETTINGS: SETTINGS_API_URL + '/get-all-settings',
    DELETE_SETTING: SETTINGS_API_URL + '/delete-setting',
    DELETE_SETTINGS: SETTINGS_API_URL + '/delete-settings'

};

export const mapStateToProps = (state: IAppStateProps) => state;