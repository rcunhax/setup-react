import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './components/App';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { user } from './reducers/user';


const store = createStore(user);

ReactDOM.render(
    <Provider store={store}><App /></Provider>, document.getElementById('root')
);
