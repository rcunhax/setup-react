import axios, { AxiosPromise } from 'axios';

export class DbyHttp {
    static doGet<T>(url: string): AxiosPromise<T> {
        // return axios.get(url);
        return axios(
            {
                method: 'get',
                url: url,
                headers: {
                    'Authorization': `Basic ${btoa('rui.cunha@pamodi.com:test')}`
                }
            });
    }

    static doDelete<T>(url: string): AxiosPromise<T> {
        return axios(
            {
                method: 'delete',
                url: url,
                headers: {
                    'Authorization': `Basic ${btoa('rui.cunha@pamodi.com:test')}`
                }
            });
    }

    static doLiferayPost<T>(cmd: string, formData: FormData, onUploadProgress: (event: any) => void = () => {}): AxiosPromise<T> {
        formData.append('cmd', `{"/${cmd}": {}}`);
        formData.append('formDate', `${Date.now()}`);
        return axios(
            {
                onUploadProgress: onUploadProgress,
                method: 'post',
                url: 'http://localhost:8080/api/jsonws/invoke',
                headers: {
                    'Authorization': `Basic ${btoa('rui.cunha@pamodi.com:test')}`,
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: formData
            });
    }
}