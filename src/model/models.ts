export interface IWizard {
    steps?: IWizardStep[];
    currentStepIndex?: number;
    currentStep?: IWizardStep;
}

export interface IWizardStep {
    step?: number;
    name?: string;
    data?: any;
    next?: (data: any) => void;
    prev?: (data: any) => void;
}

export interface ICategory {
    description?: string;
    id?: number | string;
    name?: string;
    templates?: ITemplate[];
}

export interface ISite {
    active?: boolean;
    category?: number | string;
    company?: number;
    createDate?: Date;
    description?: string;
    host?: string;
    id?: number;
    isUsingSharedHost?: boolean;
    modifiedDate?: Date;
    name?: string;
    owner?: number;
    structure?: string;
    templateId?: number | string;
    theme?: string;
    uuid?: string;
  }

export interface ITemplate {
    active?: boolean;
    category?: number | string;
    createDate?: Date;
    description?: string;
    id?: number | string;
    location?: string;
    modifiedDate?: Date;
    name?: string;
    templateFileId?: number;
    templateImageId?: number;
    uuid?: string;
    imageUrl?: string;
    imageArr?: any[];
    fileArr?: any[];
    imageFile?: File;
    fileFile?: File;
    demoUrl?: string;
}
export interface IAddress {
    street1?: string;
    street2?: string;
    street3?: string;
    city?: string;
    zip?: string;
    regionId?: number;
    countryId?: number;
    typeId?: number;
    mailing?: boolean;
    primary?: boolean;
}

export interface ISetting {
    booleanValue?: boolean;
    dateValue?: Date;
    description?: string;
    doubleValue?: number;
    id?: number;
    intValue?: number;
    longValue?: number;
    name?: string;
    stringValue?: string;
    valueType?: ISettingValueType;
    value?: string;
    deleted?: boolean;
    editingValue?: boolean;
    editingName?: boolean;
    editingType?: boolean;
}


export enum ISettingValueType {
    BOOLEAN = 'boolean',
    STRING = 'string',
    INT = 'int',
    LONG = 'long',
    DOUBLE = 'double',
    DATE = 'date'
}

export interface IUser {
    companyId?: number;
    autoPassword?: boolean;
    password?: string;
    password1?: string;
    password2?: string;
    autoScreenName?: boolean;
    screenName?: string;
    emailAddress?: string;
    facebookId?: number;
    openId?:  string;
    // locale java.util.Locale
    firstName?: string;
    middleName?: string;
    lastName?: string;
    prefixId?: number;
    suffixId?: number;
    male?: boolean;
    birthdayMonth?: number;
    birthdayDay?: number;
    birthdayYear?: number;
    jobTitle?: string;
    groupIds?: number[];
    organizationIds?: number[];
    roleIds?: number[];
    userGroupIds?: number[];
    addresses?: IAddress[];
    emailAddresses?: string[];
    // phones java.util.List
    // websites java.util.List
    // announcementsDelivers java.util.List
    sendEmail?: boolean;
}

export interface IJob {
    id?: number | string;
    name?: string;
    description?: string;
    status?: number;
    statusDetail?: string;
    items?: number;
    itemsDone?: number;
    itemsFailed?: number;
    groupId?: number;
    company?: number;
    structureId?: number;
    startDate?: Date;
    endDate?: Date;
}

export interface IAppStateProps {
    isPowerUser?: boolean;
    user?: IUser;
    isLoggedIn?: boolean;
}
export enum ActionType {
    // SET_POWER_USER = 'SET_POWER_USER',
    SET_USER = 'SET_USER',

    ADD_CATEGORY = 'ADD_CATEGORY',
    REMOVE_CATEGORY = 'REMOVE_CATEGORY',
    ADD_CATEGORIES = 'ADD_CATEGORIES',
    CATEGORY = 'SELECT_CATEGORY',
    CATEGORY_TO_EDIT = 'SELECT_CATEGORY_TO_EDIT',
    CATEGORY_TO_DELETE = 'SELECT_CATEGORY_TO_DELETE',
    WIZARD_STEP = 'WIZARD_STEP'
    // CLEAR_CATEGORY_TO_DELETE = 'CLEAR_SELECT_CATEGORY',
    // CLEAR_SELECT_CATEGORY = 'CLEAR_SELECT_CATEGORY'
}

export interface Action<T> {
    type: ActionType;
    payload: T;
}

export enum JobStatus {
    RUNNING,
    COMPLETE,
    ERROR,
    PENDING
}